import json
from typing import Optional

from fastapi import Query
from pydantic import ValidationError

from app.schemas.requests.base import Filter
from core.exceptions import UnprocessableEntity

INVALID_QUERY = "Invalid q parameter"


def parse_q(q: Optional[str] = Query(None)) -> list[Filter]:
    if q:
        try:
            filter_list = json.loads(q)
            return [Filter(**item).model_dump() for item in filter_list]
        except (ValidationError, json.JSONDecodeError) as e:
            raise UnprocessableEntity(message=INVALID_QUERY) from e
    return []
