from enum import Enum
from functools import reduce
from typing import Any, Generic, Optional, TypeVar, Union
from uuid import UUID

import sqlalchemy
from pydantic import BaseModel
from sqlalchemy import (Select, and_, between, delete, func, inspect, not_, or_, update)
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.session import object_session
from sqlalchemy.sql.expression import select

from core.database import Base
from core.database.postgres.session import BaseModel as SQLAlchemyModel
from core.exceptions import DuplicateValueException, NotFoundException

ModelType = TypeVar("ModelType", bound=Base)
PydanticType = TypeVar("PydanticType", bound=BaseModel)


class BaseRepository(Generic[ModelType]):
    """Base class for data repositories.
    This class is intended to be used as a base class for repository classes.

    Attributes:
        model_class: The model class to use.
        session: The session to use.

    Args:
        model: The model class to use.
        session: The session to use.
    """

    def __init__(self, model: type[ModelType], db_session: AsyncSession):
        self.session = db_session
        self.model_class: type[ModelType] = model

    async def get_fields(self) -> list[str]:
        """
        Returns the fields of the model.

        Returns:
            list[str]: The fields of the model.
        """
        return [column.name for column in inspect(self.model_class).columns]

    async def get_unique_values(self, field: str) -> list[Any]:
        """
        Returns all unique values for a specified field.

        Args:
            field (str): The field to get unique values for.

        Returns:
            list[Any]: The unique values for the field.
        """
        query = select(getattr(self.model_class, field)).distinct()
        result = await self.session.execute(query)
        return [row[0] for row in result]

    async def upsert(
        self,
        attributes: dict[str, Any] = None,
        pydantic_model: PydanticType | None = None,
    ) -> ModelType:
        """
        Upserts the model instance. Can upsert from either `attributes` and `pydantic` model,
        but only one can be set.

        Args:
            attributes (dict): The attributes to upsert the model with.
            pydantic_model (PydanticType): The pydantic model to upsert the model with.
        Returns:
            ModelType: The upserted model instance.
        Raises:
            TypeError: If both `attributes` and `pydantic_model` are set
        """
        if attributes is not None and pydantic_model is not None:
            raise TypeError("attributes and pydantic_model cannot be both set")

        if pydantic_model:
            model = await self.get_by([{"field": "id", "op": "eq", "value": pydantic_model.id}])

            if not model:
                model = self._entity_to_model(pydantic_model)
            else:
                updated_model_data = pydantic_model.model_dump()
                for key, value in updated_model_data.items():
                    setattr(model, key, value)
        else:
            model_id = attributes.get("id")
            model = await self.get_by([{"field": "id", "op": "eq", "value": model_id}], unique=True)

            if model:
                for key, value in attributes.items():
                    setattr(model, key, value)
            else:
                model = self.model_class(**attributes)

        self.session.add(model)
        return model

    async def count(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]] | None = None,
        join_: set[str] | None = None,
    ) -> int:
        """
        Returns the number of records matching the filter.

        Args:
            filter_list: The filter list. Uses `get_filters` to compile the filter.
            join_: The joins to make.
        Returns:
            int: The number of records.
        """
        query = self._query(join_)
        if filter_list is not None:
            query = query.where(*self.get_filters(filter_list))
        return await self._count(query)

    async def create(
        self,
        attributes: dict[str, Any] = None,
        pydantic_model: PydanticType | None = None,
    ) -> ModelType:
        """
        Creates the model instance. Can create from either `attributes` and `pydantic` model,
        but only one can be set.

        Args:
            attributes(dict): The attributes to create the model with.
            pydantic_model(PydanticType): The pydantic model to create the model with.
        Returns:
            ModelType: The created model instance.
        Raises:
            TypeError: If both `attributes` and `pydantic_model` are set
        """
        try:
            if attributes and pydantic_model:
                raise TypeError("attributes and pydantic_model cannot be both set")
            if pydantic_model:
                model = self.model_class(**pydantic_model.model_dump())
                self.session.add(model)
                return model
            if attributes is None:
                attributes = {}
            model = self.model_class(**attributes)
            self.session.add(model)
        except sqlalchemy.exc.IntegrityError as e:
            raise DuplicateValueException("Model already exists.") from e
        return model

    async def get_all(
        self,
        page: int = 1,
        limit: int = 100,
        join_: set[str] | None = None,
        sort_by: Optional[str] = None,
        order: Optional[str] = "asc",
    ) -> list[ModelType]:
        """
        Returns a list of model instances.

        Args:
            page: The page to return.
            limit: The number of records to return.
            join_: The joins to make.
            sort_by: The field to sort by.
            order: The order to sort by.
        Returns:
            list[ModelType]: The model instances
        """
        query = self._query(join_)
        if sort_by is not None:
            query = await self._sort_by(query, sort_by, order)
        if page < 0 or limit < 0:
            page, limit = 1, 100
        query = query.offset((page - 1) * limit).limit(limit)
        if join_ is not None:
            return await self._all_unique(query)
        return await self._all(query)

    async def get_by(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]],
        sort_by: Optional[str] = None,
        order: Optional[str] = "asc",
        limit: Optional[int] = 100,
        page: Optional[int] = 1,
        join_: set[str] | None = None,
        unique: bool = False,
    ) -> ModelType | list[ModelType] | None:
        """
        Returns the model instance matching the field and value.

        Args:
            filter_list: The filter list. Uses `get_filters` to compile the filter.
            sort_by: The field to sort by.
            order: The order to sort by.
            limit: The number of records to return.
            page: The page to return.
            join_: The joins to make.
            unique: Whether to return a unique model instance or a list of model instances.
                If unique is True, `_one_or_none` will be used.
                If unique is False, `_all` will be used

        Returns:
            The model instance or a list of model instances or None if the model does not exist
        """
        query = self._query(join_)
        if filter_list is not None:
            query = query.where(*self.get_filters(filter_list))
        if sort_by is not None:
            query = await self._sort_by(query, sort_by, order)
        if page < 0 or limit < 0:
            page, limit = 1, 100
        query = query.offset((page - 1) * limit).limit(limit)

        if join_ is not None:
            return await self._all_unique(query)

        return await self._one_or_none(query) if unique else await self._all(query)

    async def update(self, pydantic_model: PydanticType) -> ModelType:
        """
        Updates the model instance using a Pydantic model.

        Args:
            pydantic_model (PydanticType): The Pydantic model to update the model with.

        Returns:
            ModelType: The updated model instance.

        Raises:
            ValueError: If the model does not exist.
        """
        model_id = pydantic_model.id
        query = select(self.model_class).where(self.model_class.id == model_id)
        model: ModelType | None = await self._one_or_none(query)

        if not model:
            raise ValueError(f"Model with id {model_id} does not exist")

        updated_model_data = pydantic_model.model_dump(exclude_defaults=True, exclude_unset=True)
        updated_model_data.pop("id", None)
        for key, value in updated_model_data.items():
            setattr(model, key, value)
        return model

    async def update_many(self, data: list[dict[str, Any]]) -> None:
        """
        Update multiple rows in the table with the given data.

        Args:
            data (list[dict[str, Any]]): A list of dictionaries representing the data to be updated.
            Each dictionary should contain the 'id' and 'field' keys.

        Returns:
            None: This function does not return anything.
        """
        try:
            stmt = update(self.model_class)
            await self.session.execute(stmt, data)
        except sqlalchemy.orm.exc.StaleDataError:
            raise NotFoundException(
                f"Failed to update rows in table {self.model_class.__tablename__}.")

    async def delete(self, model: ModelType) -> None:
        """
        Deletes the model.

        Args:
            model: The model to delete.
        Returns:
            None
        """
        if object_session(model) is None:
            raise ValueError("Cannot delete instance that is not persisted")
        await self.session.delete(model)

    async def delete_by_id(self, id: UUID) -> None:
        """
        Deletes the model matching the id.

        Args:
            id: The id to match.
        Returns:
            None
        """
        query = select(self.model_class).where(self.model_class.id == id)
        model: ModelType | None = await self._one_or_none(query)
        if model:
            await self.delete(model)
        else:
            raise NotFoundException(f"Model with id {id} does not exist")

    async def delete_many(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]] | None = None,
        pydantic_models: list[PydanticType] | None = None,
        models: list[ModelType] | None = None,
    ) -> None:
        """
        Deletes multiple models. Accepts either a list of models
        or a list of pydantic models or a list of filters.
        Cannot be used with more than one parameter

        Args:
            filter_list: The filters to apply for deleting.
            pydantic_models: The pydantic models to delete.
            models: The models to delete.
        Returns:
            None
        Raises:
            TypeError: If more than one parameter is passed
            ValueError: If neither filter_list, pydantic_models, nor models are passed
        """
        param_count: int = sum(bool(x) for x in (filter_list, pydantic_models, models))
        if param_count > 1:
            raise TypeError("Only one of filter_list, pydantic_models, or models can be passed")
        elif param_count == 0:
            raise ValueError(
                "At least one of filter_list, pydantic_models, or models must be passed")

        query = delete(self.model_class)
        if filter_list:
            query = query.where(*self.get_filters(filter_list))
        elif pydantic_models:
            query = query.where(self.model_class.id.in_(x.id for x in pydantic_models))
        else:
            query = query.where(self.model_class.id.in_(x.id for x in models))

        result = await self.session.execute(query)
        if result.rowcount == 0:
            raise NotFoundException("Model wasn't found.")
        return result.rowcount

    async def upsert_many(self, data: list[dict[str, Any]]) -> None:
        """
        Upserts multiple model instances from a list of dictionaries.

        Args:
            data (list[dict]): The list of dictionaries to upsert the models from.
        """
        if not data:
            return

        table = self.model_class.__table__
        stmt = insert(table).values(data)
        do_update_stmt = stmt.on_conflict_do_update(
            index_elements=[table.c.id],
            set_={key: getattr(stmt.excluded, key)
                  for key in data[0].keys()},
        )
        await self.session.execute(do_update_stmt)

    async def _all_unique(self, query: Select) -> list[ModelType]:
        """
        Returns all results from the query. Use with join
        """
        result = await self.session.execute(query)
        return result.unique().scalars().all()

    def _query(
        self,
        join_: set[str] | None = None,
        order_: dict | None = None,
    ) -> Select:
        """
        Returns a callable that can be used to query the model.

        Args:
            join_: The joins to make.
            order_: The order of the results. (e.g desc, asc)

        Returns:
            A callable that can be used to query the model.
        """
        query = select(self.model_class)
        query = self._maybe_join(query, join_)
        query = self._maybe_ordered(query, order_)

        return query

    async def _all(self, query: Select) -> list[ModelType]:
        """
        Returns all results from the query.

        Args:
            query: The query to execute.
        Returns:
            list[ModelType]: The model instances
        """
        query = await self.session.scalars(query)
        return query.all()

    async def _first(self, query: Select) -> ModelType | None:
        """
        Returns the first result from the query.

        Args:
            query: The query to execute.
        Returns:
            The model instance
        """
        query = await self.session.scalars(query)
        return query.first()

    async def _one_or_none(self, query: Select) -> ModelType | None:
        """Returns the first result from the query or None."""
        query = await self.session.scalars(query)
        return query.one_or_none()

    async def _one(self, query: Select) -> ModelType:
        """
        Returns the first result from the query or raises NoResultFound.

        Args:
            query: The query to execute.
        Returns:
            The model instance
        """
        query = await self.session.scalars(query)
        return query.one()

    async def _count(self, query: Select) -> int:
        """
        Returns the count of the records.

        Args:
            query: The query to execute.
        Returns:
            int: The count
        """
        query = query.subquery()
        query = await self.session.scalars(select(func.count()).select_from(query))
        return query.one()

    async def _sort_by(
        self,
        query: Select,
        sort_by: str,
        order: str | None = "asc",
        model: type[ModelType] | None = None,
        case_insensitive: bool = False,
    ) -> Select:
        """
        Returns the query sorted by the given column.

        Args:
            query: The query to sort.
            sort_by: The column to sort by.
            order: The order to sort by.
            model: The model to sort by.
            case_insensitive: Whether to sort case insensitively.
        Returns:
            The query sorted by the given column.
        """
        model = model or self.model_class

        order_column = None

        if case_insensitive:
            order_column = func.lower(getattr(model, sort_by))
        else:
            order_column = getattr(model, sort_by)

        if order == "desc":
            return query.order_by(order_column.desc())

        return query.order_by(order_column.asc())

    async def _get_by(self, query: Select, field: str, value: Any) -> Select:
        """
        Returns the query filtered by the given column.

        Args:
            query: The query to filter.
            field: The field to filter by.
            value: The value to filter by.
        Returns:
            The query filtered by the given column.
        """
        return query.where(getattr(self.model_class, field) == value)

    def _maybe_join(self, query: Select, join_: set[str] | None = None) -> Select:
        """
        Returns the query with the given joins.

        Args:
            query: The query to join.
            join_: The joins to make.
        Returns:
            The query with the given joins.
        """
        if not join_:
            return query

        if not isinstance(join_, set):
            raise TypeError("join_ must be a set")

        return reduce(self._add_join_to_query, join_, query)

    def _maybe_ordered(self, query: Select, order_: dict | None = None) -> Select:
        """
        Returns the query ordered by the given column.

        Args:
            query: The query to order.
            order_: The order to order by.
        Returns:
            The query ordered by the given column.
        """
        if order_:
            if order_["asc"]:
                for order in order_["asc"]:
                    query = query.order_by(getattr(self.model_class, order).asc())
            else:
                for order in order_["desc"]:
                    query = query.order_by(getattr(self.model_class, order).desc())

        return query

    def _add_join_to_query(self, query: Select, join_: set[str]) -> Select:
        """
        Returns the query with the given join.

        Args:
            query: The query to join.
            join_: The join to make.
        Returns:
            The query with the given join.
        """
        return getattr(self, f"_join_{join_}")(query)

    def _model_to_entity(self, model_instance: ModelType,
                         entity_class: type[PydanticType]) -> PydanticType:
        """
        Converts an instance of an entitySQLAlchemy model to a Pydantic model.
        Does it recursively for nested models.

        Args:
            model_instance: The model instance.
            entity_class: The entity class.
        Returns:
            The Pydantic model
        """
        return entity_class(**self._model_to_dict(model_instance))

    def _model_to_dict(self, model: Any) -> dict:
        """
        Converts a model to a dictionary.

        :param model: The model to convert.
        :return: The dictionary.
        """
        match model:
            case None:
                return None
            case list():
                return [self._model_to_dict(item) for item in model]
            case dict():
                return {k: self._model_to_dict(v) for k, v in model.items()}
            case str() | int() | float() | bool() | bytes() | UUID():
                return model
            case BaseModel():
                return {k: self._model_to_dict(v) for k, v in model.model_dump().items()}
            case Enum():
                return model.value
            case SQLAlchemyModel():
                return {
                    k: self._model_to_dict(v)
                    for k, v in model.__dict__.items() if k not in ["_sa_instance_state"]
                }
            case _:
                return model

    def _entity_to_model(self, entity_instance: PydanticType) -> ModelType:
        """
        Converts a Pydantic model to an instance of an SQLAlchemy model.

        Args:
            entity_instance: The Pydantic model.
        Returns:
            The SQLAlchemy model
        """
        model_class: type[ModelType] = self.model_class
        model_data = {
            field: getattr(entity_instance, field)
            for field in entity_instance.__annotations__
        }
        return model_class(**model_data)

    def get_filters(self, filter_list: list[dict[str, Union[str, int, bool, list]]]):
        """
        Returns the conditions filtered by the given filters.

        Args:
            filter_list (list[dict[str, Union[str, int, bool, list]]]): The filters to apply.

        Returns:
            list: The filtered conditions.

        Examples:
            ```python
            filter_list = [
                {"field": "id", "op": "eq", "value": 1},
                {"field": "name", "op": "like", "value": "%test%"},
                {"field": "name", "op": "ne", "value": "test"},
            ]
            conditions = repository.get_filters(filter_list)
            ```
        """
        conditions = []

        for filter_dict in filter_list:
            if "container" in filter_dict:
                container = filter_dict["container"]
                values = self.get_filters(filter_dict["values"])
                container_condition = self._apply_container(container, *values)
                if container_condition is not None:
                    conditions.append(container_condition)
            else:
                field = getattr(self.model_class, filter_dict["field"])
                op = filter_dict["op"]
                value = filter_dict["value"]
                operation_condition = self._apply_operation(field, op, value)
                if operation_condition is not None:
                    conditions.append(operation_condition)

        return conditions

    def _apply_container(self, container: str, *values):
        match container:
            case "not":
                return not_(*values)
            case "and":
                return and_(*values)
            case "or":
                return or_(*values)
            # TODO: Add more containers here when needed (e.g. "aggregate")
            case _:
                return

    def _apply_operation(self, field, op: str, value: Union[str, int, bool, list]):
        match op:
            case "eq":
                return field == value
            case "ne":
                return field != value
            case "lt":
                return field < value
            case "gt":
                return field > value
            case "lte":
                return field <= value
            case "gte":
                return field >= value
            case "like":
                return field.like(value)
            case "in":
                return field.in_(value)
            case "btw_i" | "btw_d":
                return between(field, value["from"], value["to"])
            # TODO: Add more operations here when needed
            case _:
                return
