from typing import Any, Generic, Optional, Union
from uuid import UUID

from pydantic import BaseModel

from core.database import Propagation, Transactional
from core.exceptions import NotFoundException
from core.repository.base import BaseRepository, ModelType, PydanticType


class BaseController(Generic[ModelType]):
    """Base class for controllers.
    This class is intended to be used as a base class for controller classes.

    Attributes:
        repository: The repository to use.

    Args:
        repository: The repository to use.
    """

    def __init__(self, model: type[ModelType], repository: BaseRepository):
        self.model_class: type[ModelType] = model
        self.repository: BaseRepository = repository

    @Transactional(propagation=Propagation.REQUIRED)
    async def count(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]] | None = None,
        join_: set[str] | None = None,
    ) -> int:
        """
        Returns the number of records matching the filter.

        Args:
            filter_list: The filter list. Uses `get_filters` to compile the filter.
            join_: The joins to make.
        Returns:
            int: The number of records.
        """
        return await self.repository.count(filter_list, join_)

    @Transactional(propagation=Propagation.REQUIRED)
    async def create(
        self,
        attributes: dict[str, Any] = None,
        pydantic_model: PydanticType | None = None,
    ) -> ModelType:
        """
        Creates the model instance. Can create from either `attributes` and `pydantic` model,
        but only one can be set.

        Args:
            attributes(dict): The attributes to create the model with.
            pydantic_model(PydanticType): The pydantic model to create the model with.
        Returns:
            ModelType: The created model instance.
        Raises:
            TypeError: If both `attributes` and `pydantic_model` are set
        """
        return await self.repository.create(attributes, pydantic_model)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update(self, pydantic_model: PydanticType) -> PydanticType:
        """
        Updates the model instance using a Pydantic model.

        Args:
            pydantic_model (PydanticType): The Pydantic model to update the model with.

        Returns:
            ModelType: The updated model instance.

        Raises:
            ValueError: If the model does not exist.
        """
        return await self.repository.update(pydantic_model=pydantic_model)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update_many(self, data: list[dict[str, Any]]) -> None:
        """
        Update multiple rows in the table with the given data.

        Args:
            data (list[dict[str, Any]]): A list of dictionaries representing the data to be updated.
            Each dictionary should contain the 'id' and 'field' keys.

        Returns:
            None: This function does not return anything.
        """
        await self.repository.update_many(data)

    @Transactional(propagation=Propagation.REQUIRED)
    async def upsert(
        self,
        attributes: dict[str, Any] | None = None,
        pydantic_model: PydanticType | None = None,
    ) -> ModelType:
        """
        Upserts the model instance. Can upsert from either `attributes` and `pydantic` model,
        but only one can be set.

        Args:
            attributes (dict): The attributes to upsert the model with.
            pydantic_model (PydanticType): The pydantic model to upsert the model with.
        Returns:
            ModelType: The upserted model instance.
        Raises:
            TypeError: If both `attributes` and `pydantic_model` are set
        """
        return await self.repository.upsert(attributes, pydantic_model)

    @Transactional(propagation=Propagation.REQUIRED)
    async def upsert_many(self, data: list[dict[str, Any]]) -> None:
        """
        Upserts multiple model instances from a list of dictionaries.

        Args:
            data (list[dict]): The list of dictionaries to upsert the models from.
        """
        await self.repository.upsert_many(data)

    @Transactional(propagation=Propagation.REQUIRED)
    async def get_all(
        self,
        page: int = 1,
        limit: int = 100,
        join_: set[str] | None = None,
        sort_by: Optional[str] = None,
        order: Optional[str] = "asc",
    ) -> list[ModelType]:
        """
        Returns a list of model instances.

        Args:
            page: The page to return.
            limit: The number of records to return.
            join_: The joins to make.
            sort_by: The field to sort by.
            order: The order to sort by.
        Returns:
            list[ModelType]: The model instances
        """
        return await self.repository.get_all(page, limit, join_, sort_by, order)

    async def get_by(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]],
        sort_by: Optional[str] = None,
        order: Optional[str] = "asc",
        limit: Optional[int] = 100,
        page: Optional[int] = 1,
        join_: set[str] | None = None,
        unique: bool = False,
        pydantic_model: PydanticType | None = None,
    ) -> ModelType | list[ModelType] | None:
        """
        Returns the model instance matching the field and value.

        Args:
            filter_list: The filter list. Uses `get_filters` to compile the filter.
            sort_by: The field to sort by.
            order: The order to sort by.
            limit: The number of records to return.
            page: The page to return.
            join_: The joins to make.
            unique: Whether to return a unique model instance or a list of model instances.
                If unique is True, `_one_or_none` will be used.
                If unique is False, `_all` will be used

        Returns:
            The model instance or a list of model instances or None if the model does not exist
        """
        result = await self.repository.get_by(filter_list, sort_by, order, limit, page, join_,
                                              unique)
        if not pydantic_model:
            return result
        return [self.repository._model_to_entity(model, pydantic_model) for model in result]

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete_many(
        self,
        filter_list: list[dict[str, Union[str, int, bool, list]]] | None = None,
        pydantic_models: list[PydanticType] | None = None,
        models: list[ModelType] | None = None,
    ) -> None:
        """
        Deletes multiple models. Accepts either a list of models
        or a list of pydantic models or a list of filters.
        Cannot be used with more than one parameter

        Args:
            filter_list: The filters to apply for deleting.
            pydantic_models: The pydantic models to delete.
            models: The models to delete.
        Returns:
            None
        Raises:
            TypeError: If more than one parameter is passed
            ValueError: If neither filter_list, pydantic_models, nor models are passed
        """
        return await self.repository.delete_many(filter_list, pydantic_models, models)

    @Transactional(propagation=Propagation.REQUIRED)
    async def get_by_id(self,
                        id: UUID,
                        join_: set[str] | None = None,
                        as_dict: bool = False) -> ModelType | dict:
        """
        Returns the model instance matching the id.

        Args:
            id: The id of the model to return.
            join_: The joins to make.
            as_dict: Whether to return the model as a dictionary.
        Returns:
            ModelType: The model instance or as a dictionary
        Raises:
            NotFoundException: If the model does not exist
        """
        model = await self.repository.get_by(filter_list=[{
            "field": "id",
            "op": "eq",
            "value": id
        }],
                                             join_=join_)
        if not model:
            raise NotFoundException(message="Model not found")
        if as_dict:
            return (self.model_to_dict(model[0])
                    if isinstance(model, list) else self.model_to_dict(model))
        return model[0] if isinstance(model, list) else model

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete(self, model: ModelType) -> None:
        """
        Deletes the model.

        Args:
            model: The model to delete.
        Returns:
            None
        """
        await self.repository.delete(model)

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete_by_id(self, id: UUID) -> None:
        """
        Deletes the model matching the id.

        Args:
            id: The id to match.
        Returns:
            None
        """
        await self.repository.delete_by_id(id)

    @Transactional(propagation=Propagation.REQUIRED)
    async def get_unique_values(self, field: str) -> list[Any]:
        """
        Returns all unique values for a specified field.

        Args:
            field (str): The field to get unique values for.

        Returns:
            list[Any]: The unique values for the field.
        """
        return await self.repository.get_unique_values(field)

    async def get_fields(self) -> list[str]:
        """
        Returns the fields of the model.

        Returns:
            list[str]: The fields of the model.
        """
        return await self.repository.get_fields()

    @staticmethod
    async def extract_attributes_from_schema(schema: BaseModel,
                                             excludes: set = None) -> dict[str, Any]:
        """
        Extracts the attributes from the schema.

        :param schema: The schema to extract the attributes from.
        :param excludes: The attributes to exclude.
        :return: The attributes.
        """

        return await schema.dict(exclude=excludes, exclude_unset=True)

    def model_to_dict(self, model: Any) -> dict:
        """
        Converts a model to a dictionary.

        :param model: The model to convert.
        :return: The dictionary.
        """
        return self.repository._model_to_dict(model)
