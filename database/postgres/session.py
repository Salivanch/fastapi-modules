from contextvars import ContextVar, Token
from typing import Union

from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import (AsyncSession, async_scoped_session, create_async_engine)
from sqlalchemy.orm import (Session, declarative_base, scoped_session, sessionmaker)
from sqlalchemy.sql.expression import Delete, Insert, Update

from core.config import config

session_context: ContextVar[str] = ContextVar("session_context")


def get_session_context() -> str:
    return session_context.get()


def set_session_context(session_id: str) -> Token:
    return session_context.set(session_id)


def reset_session_context(context: Token) -> None:
    session_context.reset(context)


url = config.POSTGRES_URL if config.IS_DOCKER else config.POSTGRES_URL_LOCAL
engines = {
    "writer":
    create_async_engine(
        url,
        pool_recycle=3600,
        connect_args={"server_settings": {
            "jit": "off"
        }},
    ),
    "reader":
    create_async_engine(
        url,
        pool_recycle=3600,
        connect_args={"server_settings": {
            "jit": "off"
        }},
    ),
}
url_sync = config.POSTGRES_URL_SYNC
engines_sync = {
    "writer":
    create_engine(
        url_sync,
        pool_recycle=3600,
        connect_args={"server_settings": {
            "jit": "off"
        }},
    ),
    "reader":
    create_engine(
        url_sync,
        pool_recycle=3600,
        connect_args={"server_settings": {
            "jit": "off"
        }},
    ),
}


class RoutingSession(Session):

    def get_bind(self, mapper=None, clause=None, **kwargs):
        if self._flushing or isinstance(clause, (Update, Delete, Insert)):
            return engines["writer"].sync_engine
        return engines["reader"].sync_engine


class SyncRoutingSession(Session):

    def get_bind(self, mapper=None, clause=None, **kwargs):
        if self._flushing or isinstance(clause, (Update, Delete, Insert)):
            return engines_sync["writer"]
        return engines_sync["reader"]


async_session_factory = sessionmaker(
    class_=AsyncSession,
    sync_session_class=RoutingSession,
    expire_on_commit=False,
)
sync_session_factory = sessionmaker(
    class_=SyncRoutingSession,
    expire_on_commit=False,
)

session: Union[AsyncSession, async_scoped_session] = async_scoped_session(
    session_factory=async_session_factory,
    scopefunc=get_session_context,
)

sync_session: Union[Session, scoped_session] = scoped_session(
    session_factory=sync_session_factory,
    scopefunc=get_session_context,
)


async def get_session():
    """
    Get the database session.
    This can be used for dependency injection.

    :return: The database session.
    """
    try:
        yield session
    finally:
        await session.close()


MODELS = {}


class BaseModel:

    @classmethod
    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        if cls.__name__ != "Base":
            MODELS[cls.__tablename__] = cls

    @classmethod
    def get_public_fields(cls) -> list[str]:
        return [c.key for c in cls.__table__.columns if c.key != "id"]


Base = declarative_base(cls=BaseModel)
