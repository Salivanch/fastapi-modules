import re
from enum import Enum
from functools import wraps

from sqlalchemy.exc import IntegrityError

from core.database.postgres import session
from core.exceptions.base import DuplicateValueException, NotFoundException


class Propagation(Enum):
    REQUIRED = "required"
    REQUIRED_NEW = "required_new"


class Transactional:

    def __init__(self, propagation: Propagation = Propagation.REQUIRED):
        self.propagation = propagation

    def __call__(self, function):

        @wraps(function)
        async def decorator(*args, **kwargs):
            try:
                if self.propagation == Propagation.REQUIRED:
                    result = await self._run_required(
                        function=function,
                        args=args,
                        kwargs=kwargs,
                    )
                elif self.propagation == Propagation.REQUIRED_NEW:
                    result = await self._run_required_new(
                        function=function,
                        args=args,
                        kwargs=kwargs,
                    )
                else:
                    result = await self._run_required(
                        function=function,
                        args=args,
                        kwargs=kwargs,
                    )
            except IntegrityError as e:
                await session.rollback()
                error_message = str(e)
                if "duplicate key value violates unique constraint" in error_message:
                    match = re.search(r"DETAIL:\s+Key \((.*?)\)=", error_message)
                    if match:
                        duplicate_field = match[1]
                        raise DuplicateValueException(
                            f"Unique constraint violated: {duplicate_field} already exists.")
                    else:
                        print(e)
                        raise DuplicateValueException("A unique constraint was violated.")
                if "violates not-null constraint" in error_message:
                    print(error_message)
                    match = re.search(r'null value in column (".+") of relation', error_message)
                    if match:
                        duplicate_field = match[1]
                        raise NotFoundException(f"Field {duplicate_field} cannot be null.")
                    raise NotFoundException("Field cannot be null.")
                if "violates foreign key constraint" in error_message:
                    match = re.search(r"DETAIL:\s+Key \((.*?)\)=", error_message)
                    if match:
                        duplicate_field = match[1]
                        raise NotFoundException(
                            f"Foreign key constraint violated: {duplicate_field} does not exist.")
                    raise NotFoundException(str(e))
                    print(e)
                    raise DuplicateValueException("Model already exists.") from e
            except Exception as exception:
                await session.rollback()
                raise exception

            return result

        return decorator

    async def _run_required(self, function, args, kwargs) -> None:
        result = await function(*args, **kwargs)
        await session.commit()
        return result

    async def _run_required_new(self, function, args, kwargs) -> None:
        session.begin()
        result = await function(*args, **kwargs)
        await session.commit()
        return result
